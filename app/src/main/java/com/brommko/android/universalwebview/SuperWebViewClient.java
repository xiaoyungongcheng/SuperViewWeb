package com.brommko.android.universalwebview;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.brommko.android.universalwebview.util.NetworkHandler;
import com.brommko.android.universalwebview.util.ProgressDialogHelper;
import com.brommko.android.universalwebview.util.UrlHander;

public class SuperWebViewClient extends WebViewClient {


    private Main2Activity activity;
    public SuperWebViewClient(Main2Activity activity){
        this.activity=activity;
    }
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        String host = Uri.parse(url).getHost();
        activity.urlData = url;
        boolean result = UrlHander.checkUrl(activity, url);
        if (result) {
            ProgressDialogHelper.dismissProgress();
        } else {
            activity.currentUrl = url;
            if (!activity.show_content) {
                ProgressDialogHelper.showProgress(activity);
            }
        }
        return result;
//        if (!url.startsWith("http")) {
//
//            try {
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                activity.startActivity(intent);
//            } catch (ActivityNotFoundException e) {
//                e.printStackTrace();
//                if (url.startsWith("d2k")||url.startsWith("magnet")||url.startsWith("thunder")||url.startsWith("flashget")||
//                        url.startsWith("qqdl")){
//                    Toast.makeText(activity,"请安装迅雷",0).show();
//
//                }else if (url.startsWith("baiduyun")){
//                    Toast.makeText(activity,"请安装百度云盘",0).show();
//                }
//                Log.i("no","noactivity");
//            }
//            return true;
//        }else {
//            view.loadUrl(url);
//        }
//        return false;
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
            view.loadUrl("file:///android_asset/NoInternet.html");
        }
        activity.hideStatusBar();
        ProgressDialogHelper.dismissProgress();
        handler.proceed();
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
            view.loadUrl("file:///android_asset/NoInternet.html");
        }
        activity.hideStatusBar();
        ProgressDialogHelper.dismissProgress();
    }

    @Override
    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
        super.onReceivedHttpError(view, request, errorResponse);
        if (!NetworkHandler.isNetworkAvailable(view.getContext())) {
            view.loadUrl("file:///android_asset/NoInternet.html");
        }
        activity.hideStatusBar();
        ProgressDialogHelper.dismissProgress();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        activity.showContent();
        activity.setToolbarButtonColor();
        activity.hideStatusBar();
        ProgressDialogHelper.dismissProgress();

//            view.getSettings().setBlockNetworkImage(false);
//            if (!view.getSettings().getLoadsImagesAutomatically()) {
//                view.getSettings().setLoadsImagesAutomatically(true);
//            }
    }

    @Override
    public void onPageCommitVisible(WebView view, String url) {
        super.onPageCommitVisible(view, url);
        activity.setToolbarButtonColor();
        activity.hideStatusBar();
        ProgressDialogHelper.dismissProgress();
    }
}
